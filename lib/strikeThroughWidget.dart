import 'package:flutter/material.dart';

// ignore: camel_case_types
class strikeThrough extends StatelessWidget {
  final bool todoToggle;
  final String todoText;
  final Color color;
  strikeThrough({this.todoToggle, this.todoText, this.color}) : super();

  Widget _strikeWidget() {
    if (todoToggle == false) {
      return Text(
        todoText,
        style: TextStyle(
            fontSize: 22.0, fontWeight: FontWeight.w600, color: color),
      );
    } else {
      return Text(
        todoText,
        style: TextStyle(
          decoration: TextDecoration.lineThrough,
          color: Colors.blueGrey[800],
          fontStyle: FontStyle.italic,
          fontWeight: FontWeight.w800,
          fontSize: 22.0,
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return _strikeWidget();
  }
}
