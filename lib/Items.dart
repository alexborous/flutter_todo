import 'package:flutter/material.dart';
import 'package:json_string/json_string.dart';

class Items with Jsonable {
  bool value;
  Text text;
  bool notificationable;
  int textColor;
  int year;
  int month;
  int day;
  int hour;
  int minute;
  String textCategory;
  int categoryColor;
  Color getColor() => text.style.color;
  String getText() => text.data;
  String jsonText;
  Items(
      {this.text,
      this.value,
      this.notificationable = false,
      this.jsonText,
      this.textCategory = "All",
      this.textColor,
      this.year = 0,
      this.categoryColor,
      this.hour = 0,
      this.day = 0,
      this.minute = 0,
      this.month = 0}) {
    jsonText = text.data;
  }

  factory Items.fromJson(Map<String, dynamic> json) => Items(
      value: json['value'],
      jsonText: json['text'],
      textCategory: json['textCategory'],
      textColor: json['textColor'],
      notificationable: json['notificationable'],
      year: json['year'],
      month: json['month'],
      day: json['day'],
      hour: json['hour'],
      minute: json['minute'],
      categoryColor: json['categoryColor']);

  Map<String, dynamic> toJson() => {
        'value': this.value.toString(),
        'text': this.jsonText,
        'textCategory': this.textCategory,
        'textColor': this.textColor,
        'notificationable': this.notificationable.toString(),
        'year': this.year,
        'month': this.month,
        'day': this.day,
        'hour': this.hour,
        'minute': this.minute,
        'categoryColor': this.categoryColor,
      };
}
