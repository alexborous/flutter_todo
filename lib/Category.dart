import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:json_string/json_string.dart';
import 'package:path_provider/path_provider.dart';
import 'Items.dart';

class Category {
  List getCategory(int index) => categories.elementAt(index);

  Future delete(int index) async {
    categories.elementAt(index).forEach((element) => all.remove(element));
    categories.removeAt(index);
    colors.removeAt(index);
    names.removeAt(index);
  }

  Category();
  // ignore: non_constant_identifier_names
  String getName(int index) => names.elementAt(index);
  List<String> names = [];
  List<Color> colors = [];

  Future rebuildList() async {
    if (categories.length == 1) return categories;
    categories.removeLast();
    categories.forEach((element) {
      element.forEach((item) {
        all.remove(item);
      });
      all = all + element;
    });

    categories.add(all);
  }

  void todayTasks() {
    categories.forEach((list) {
      list.forEach((item) {
        if (item.day == DateTime.now().day &&
            item.month == DateTime.now().month &&
            item.year == DateTime.now().year) today.add(item);
      });
    });
    categories.insert(0, today);
    names.insert(0, "Today");
    colors.insert(0, Colors.greenAccent);
  }

  static List<Items> today = [];
  static List<Items> temp = [
    Items(
        value: false,
        categoryColor: Colors.deepPurple.value,
        jsonText:
            "Press the action button in the bottom right corner to create a task",
        text: Text(
          "Press the action button in the bottom right corner to create a task",
          style: TextStyle(color: Colors.black),
        ),
        textCategory: "All",
        textColor: Colors.black.value)
  ];
  static List<Items> all = [
    Items(
        jsonText: "ok",
        text: Text(
          "ok",
          style: TextStyle(color: Colors.green),
        ),
        textCategory: "All",
        textColor: Colors.green.value)
  ];
  List<List<Items>> categories = [all];
  Category.fromJson(List<List<dynamic>> usersJson)
      : categories = usersJson
            .map((user) => user.map((e) => Items.fromJson(e)))
            .toList();

  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();
    print("Directory: $directory");
    return directory.path;
  }

  Future<bool> initializeLists() async {
    this.categories = await readList();

    if (this.categories != []) {
      return true;
    }
    return false;
  }

  Future<File> get _localFile async {
    final path = await _localPath;
    print("Path $path");
    return File('$path/category.txt');
  }

  Future<List<List<Items>>> readList() async {
    try {
      final file = await _localFile;
      all = [];
      categories = [all];
      names = ["All"];
      colors = [Colors.deepPurple];
      today = [];
      List<dynamic> items = [];
      // Read the file
      var contents = file.readAsStringSync();

      items = jsonDecode(contents);
      items.forEach((key) {
        print("KEY:$key");
        var itemTemp = Items(
            value: key['value'] == "true" ? true : false,
            text: Text(
              key['text'],
              style: TextStyle(color: Color(key['textColor']) ?? Colors.black),
            ),
            notificationable: key['notificationable'] == "true" ? true : false,
            textCategory: key['textCategory'],
            textColor: key['textColor'] ?? Colors.blueGrey.value,
            year: key['year'] ?? 0,
            month: key['month'] ?? 0,
            day: key['day'] ?? 0,
            hour: key['hour'] ?? 0,
            minute: key['minute'] ?? 0,
            categoryColor: key['categoryColor'] ?? Colors.indigoAccent.value);
        if (names.indexOf(itemTemp.textCategory) == -1) {
          names.insert(names.length - 1, itemTemp.textCategory);
          colors.insert(colors.length - 1, Color(itemTemp.categoryColor));
          List<Items> tempList = [];
          tempList.add(itemTemp);
          categories.insert(categories.length - 1, tempList);
        } else {
          categories[names.indexOf(itemTemp.textCategory)].add(itemTemp);
          print("length:${categories.length}");
        }
      });
      print("Names : $names");
      rebuildList();
      return categories;
    } catch (e) {
      // If encountering an error, return 0
      print("Error in readlist : $e");
      rebuildList();
      return categories;
    }
  }

  Future<File> writeList(List<List<Items>> list) async {
    final file = await _localFile;

    list.forEach((element) async {
      final jsonString = JsonString.encodeObjectList(element);
      print("JsontoSting(source) : ${jsonString.source} ");

      file.writeAsStringSync(jsonString.source);
    });
    print("File:${file.toString()}");
    // Write the file

    return file;
  }
}
