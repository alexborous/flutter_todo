import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'Category.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_todoapp/Category.dart';
import 'package:flutter_todoapp/Items.dart';
import 'package:states_rebuilder/states_rebuilder.dart';
import 'slidingWidgets.dart';
import 'createAndModifyTask.dart';
import 'strikeThroughWidget.dart';

bool toBeUpdated = false;
bool loading = true;
void main() => runApp(TODOApp());
FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();

class TODOApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Injector(
        inject: [Inject(() => Category())],
        builder: (context) {
          return TODO();
        });
  }
}

class TODO extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return TODOState();
  }
}

class TODOState extends State<TODO> with WidgetsBindingObserver {
  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    super.dispose();
    WidgetsBinding.instance.removeObserver(this);
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    switch (state) {
      case AppLifecycleState.inactive:
        print("Inactive");
        setState(() {
          toBeUpdated = true;
        });
        break;

      case AppLifecycleState.paused:
        setState(() {
          toBeUpdated = true;
        });
        print("Paused");
        break;
      case AppLifecycleState.resumed:
        print("Resumed");
        break;
      case AppLifecycleState.detached:
        print("Suspending");
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return WhenRebuilder<Category>(
        dispose: (context, modelRM) => modelRM.setState((s) async =>
            await modelRM.state.writeList(modelRM.state.categories)),
        observe: () => RM.get<Category>(),
        initState: (context, model) {
          model.setState((s) async {
            await model.state.initializeLists();
            model.state.todayTasks();
          });
          loading = false;
        },
        onIdle: () => Center(child: CircularProgressIndicator()),
        onWaiting: () => Center(child: CircularProgressIndicator()),
        onError: (error) => Center(child: CircularProgressIndicator()),
        onData: (data) => loading
            ? CircularProgressIndicator()
            : MaterialApp(
                debugShowCheckedModeBanner: false,
                theme: ThemeData(
                    primaryColor: Colors.white,
                    accentColor: Colors.blueGrey.shade700,
                    textTheme: TextTheme(
                        bodyText1: TextStyle(color: Colors.blueGrey[300]))),
                initialRoute: '/',
                routes: {
                  '/': (context) => App(),
                  // Passing our function as a callback
                },
              ));
  }
}

class App extends StatefulWidget {
  @override
  AppState createState() {
    return AppState();
  }
}

class AppState extends State<App> {
  final TextEditingController controller = TextEditingController();
  int index = 0;
  var initializationSettingsAndroid;
  var initializationSettingsIos;
  var initializationSettings;
  Color pickerColor = Colors.deepOrange;
  Color currentColor = Colors.deepOrange;
  ScrollController scrollController = ScrollController();
// ValueChanged<Color> callback
  void changeColor(Color color) {
    setState(() => pickerColor = color);
  }

  // Function that modifies the state when a new task is created
  @override
  void initState() {
    super.initState();
    initializationSettingsAndroid =
        AndroidInitializationSettings('@mipmap/ic_launcher');
    initializationSettingsIos = IOSInitializationSettings(
        onDidReceiveLocalNotification: onDidReceiveLocalNotification);
    initializationSettings = InitializationSettings(
        initializationSettingsAndroid, initializationSettingsIos);
    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: onSelectNotification);
  }

  Future onSelectNotification(String payload) async {
    if (payload != null) {
      debugPrint('Notification payload: $payload');
    }
  }

  Future onDidReceiveLocalNotification(
      int id, String title, String body, String payload) async {
    await showDialog(
        context: context,
        builder: (BuildContext context) => CupertinoAlertDialog(
              title: Text(title),
              content: Text(body),
              actions: <Widget>[
                CupertinoDialogAction(
                  isDefaultAction: true,
                  child: Text('Ok'),
                  onPressed: () async {
                    Navigator.of(context, rootNavigator: true).pop();
                    await Navigator.push(context,
                        MaterialPageRoute(builder: (context) => App()));
                  },
                )
              ],
            ));
  }

  Widget build(BuildContext context) {
    return StateBuilder<Category>(
        observe: () => RM.get<Category>(),
        builder: (context, modelRM) {
          if (toBeUpdated) {
            modelRM.setState((s) async =>
                await modelRM.state.writeList(modelRM.state.categories));
            toBeUpdated = false;
          }
          if (modelRM.isWaiting)
            return Center(
              child: CircularProgressIndicator(),
            );
          if (!toBeUpdated)
            return Scaffold(
                appBar: AppBar(
                  centerTitle: false,
                  backgroundColor: Theme.of(context).primaryColor,
                  title: Text(
                    '       ${modelRM.state.names[index]}',
                    style: TextStyle(
                        fontSize: 30.0,
                        fontWeight: FontWeight.w800,
                        color: Colors.black),
                  ),
                  toolbarHeight: MediaQuery.of(context).size.height * 0.08,
                  elevation: 0.0,
                ),
                backgroundColor: Theme.of(context).primaryColor,
                body: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: ReorderableListView(
                        scrollController: scrollController,
                        children: <Widget>[
                          for (final item
                              in modelRM.state.categories[index] ?? [])
                            Padding(
                              key: Key(item.text.data),
                              padding: const EdgeInsets.all(2.0),
                              child: Container(
                                key: Key(item.text.data),
                                padding: EdgeInsets.symmetric(
                                    vertical: 4.0, horizontal: 3.0),
                                decoration: BoxDecoration(
                                  color: Theme.of(context).primaryColor,
                                  border: Border(
                                      bottom:
                                          BorderSide(color: Colors.grey[300])),
                                ),
                                child: Dismissible(
                                  key: Key(item.text.data),
                                  secondaryBackground: slideLeftBackground(),
                                  background: slideRightBackground(),
                                  child: Padding(
                                    padding: const EdgeInsets.only(top: 6.0),
                                    child: CheckboxListTile(
                                      controlAffinity:
                                          ListTileControlAffinity.platform,
                                      activeColor: Colors.blueGrey,
                                      checkColor:
                                          Theme.of(context).primaryColor,
                                      subtitle: item.notificationable &&
                                              !item.value
                                          ? Row(children: [
                                              Icon(
                                                Icons.alarm,
                                                color: Colors.grey,
                                                size: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.05,
                                              ),
                                              Text(
                                                item.minute < 10
                                                    ? "   ${item.day}/${item.month}/${item.year} ${item.hour}:0${item.minute}"
                                                    : "   ${item.day}/${item.month}/${item.year} ${item.hour}:${item.minute}" ??
                                                        " ",
                                                style: TextStyle(
                                                    color: Colors.grey,
                                                    fontStyle: FontStyle.italic,
                                                    fontWeight:
                                                        FontWeight.w600),
                                              ),
                                            ])
                                          : null,
                                      secondary: Padding(
                                        padding: const EdgeInsets.all(12.0),
                                        child: Container(
                                          width: 10.0,
                                          height: 10.0,
                                          decoration: BoxDecoration(
                                              color: modelRM.state.colors[
                                                  modelRM.state.names.indexOf(
                                                      item.textCategory)],
                                              shape: BoxShape.circle),
                                        ),
                                      ),
                                      value: item.value,
                                      onChanged: (bool) {
                                        setState(() {
                                          if (!bool) {
                                            item.value = false;
                                          } else {
                                            if (item.notificationable) {
                                              FlutterLocalNotificationsPlugin()
                                                  .cancel(
                                                      item.text.data.length);
                                              item.notificationable = false;
                                            }
                                            item.value = true;
                                          }
                                        });
                                      },
                                      key: Key(item.text.data),
                                      title: strikeThrough(
                                        todoText: item.text.data,
                                        todoToggle: item.value,
                                        color: item.text.style.color ??
                                            Colors.teal,
                                      ),
                                    ),
                                  ),
                                  confirmDismiss: (direction) async {
                                    if (direction ==
                                        DismissDirection.endToStart) {
                                      modelRM.setState((something) {
                                        modelRM.state.categories
                                            .forEach((element) {
                                          element.removeWhere((element) =>
                                              element.text == item.text);
                                        });
                                      });
                                      if (item.notificationable)
                                        FlutterLocalNotificationsPlugin()
                                            .cancel(item.text.data.length);
                                      Flushbar(
                                        message: "Task removed",
                                        icon: Icon(
                                          Icons.info_outline,
                                          size: 28,
                                          color: Colors.blue.shade300,
                                        ),
                                        leftBarIndicatorColor:
                                            Colors.blue.shade300,
                                        duration: Duration(seconds: 3),
                                      )..show(context);
                                      return true;
                                    } else {
                                      if (item.value == false) {
                                        Navigator.of(context).push(
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    TODOModify(item, index)));
                                      } else
                                        Flushbar(
                                          message:
                                              "Cannot modify completed task",
                                          icon: Icon(
                                            Icons.error_outline,
                                            size: 28,
                                            color: Colors.red.shade800,
                                          ),
                                          duration: const Duration(seconds: 3),
                                        )..show(context);
                                      return false;
                                    }
                                  },
                                ),
                              ),
                            )
                        ],
                        // ignore: non_constant_identifier_names
                        onReorder: (OldIndex, NewIndex) {
                          setState(() {
                            if (OldIndex < NewIndex) {
                              NewIndex -= 1;
                            }

                            var getReplacedWidget = modelRM
                                .state.categories[index]
                                .removeAt(OldIndex);
                            modelRM.state.categories[index]
                                .insert(NewIndex, getReplacedWidget);
                          });
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 15.0),
                      child: Text(
                        "Categories",
                        style: TextStyle(
                            color: Colors.grey, fontWeight: FontWeight.w600),
                      ),
                    ),
                    Expanded(
                      child: SingleChildScrollView(
                        key: Key("Second"),
                        scrollDirection: Axis.vertical,
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              for (final i in modelRM.state.categories)
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 5.0, vertical: 8.0),
                                  child: Scrollbar(
                                    child: Row(children: [
                                      GestureDetector(
                                        onTap: () {
                                          final buttonIndex = modelRM
                                              .state.categories
                                              .indexOf(i);
                                          modelRM.setState(
                                              (s) => index = buttonIndex);
                                        },
                                        child: Container(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.7,
                                          padding: EdgeInsets.symmetric(
                                              vertical: 10.0, horizontal: 5.0),
                                          decoration: BoxDecoration(
                                              color: modelRM.state.colors
                                                  .elementAt(modelRM
                                                      .state.categories
                                                      .indexOf(i)),
                                              borderRadius:
                                                  BorderRadius.circular(12.0)),
                                          child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          left: 8.0),
                                                  child: Text(
                                                    modelRM.state.names[modelRM
                                                        .state.categories
                                                        .indexOf(i)],
                                                    textAlign: TextAlign.start,
                                                    style: TextStyle(
                                                        color: Theme.of(context)
                                                            .primaryColor,
                                                        fontSize: 20.0),
                                                  ),
                                                ),
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          left: 8.0),
                                                  child: Text(
                                                    (modelRM
                                                                .state
                                                                .categories[modelRM
                                                                    .state
                                                                    .categories
                                                                    .indexOf(i)]
                                                                .length !=
                                                            1)
                                                        ? "${modelRM.state.categories[modelRM.state.categories.indexOf(i)].length} Tasks"
                                                        : "${modelRM.state.categories[modelRM.state.categories.indexOf(i)].length} Task",
                                                    style: TextStyle(
                                                        color: Colors.white70,
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                )
                                              ]),
                                        ),
                                      ),
                                      IconButton(
                                          onPressed: () {
                                            final buttonIndex = modelRM
                                                .state.categories
                                                .indexOf(i);
                                            modelRM.setState((s) {
                                              index = buttonIndex;
                                            });
                                            Navigator.of(context).push(
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        TODOCreate(index)));
                                          },
                                          icon: Icon(
                                            Icons.add_circle_rounded,
                                            size: MediaQuery.of(context)
                                                    .size
                                                    .height *
                                                0.045,
                                            color:
                                                Theme.of(context).accentColor,
                                          )),
                                      if (modelRM.state.names[modelRM
                                              .state.categories
                                              .indexOf(i)] !=
                                          "All")
                                        IconButton(
                                          icon: Icon(
                                            Icons.delete,
                                            size: MediaQuery.of(context)
                                                    .size
                                                    .height *
                                                0.045,
                                            color:
                                                Theme.of(context).accentColor,
                                          ),
                                          onPressed: () {
                                            final buttonIndex = modelRM
                                                .state.categories
                                                .indexOf(i);

                                            modelRM.setState((s) async {
                                              index = buttonIndex;
                                              await modelRM.state.delete(index);
                                              await modelRM.state.rebuildList();
                                            });
                                          },
                                        )
                                    ]),
                                  ),
                                )
                            ]),
                      ),
                    ),
                  ],
                ),
                floatingActionButton: FloatingActionButton(
                    child: Icon(Icons.create_new_folder,
                        size: 35.0, color: Theme.of(context).primaryColor),
                    onPressed: () async {
                      showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return Dialog(
                              backgroundColor: Colors.blueGrey.shade900,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(
                                      20.0)), //this right here
                              child: Container(
                                height: 200,
                                width: MediaQuery.of(context).size.width * 0.6,
                                child: Padding(
                                  padding: const EdgeInsets.all(12.0),
                                  child: Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      GestureDetector(
                                        onTap: () {
                                          showDialog(
                                            context: context,
                                            child: AlertDialog(
                                              title:
                                                  const Text('Pick a color!'),
                                              content: SingleChildScrollView(
                                                child: ColorPicker(
                                                  pickerColor: pickerColor,
                                                  onColorChanged: changeColor,
                                                  showLabel: true,
                                                  pickerAreaHeightPercent: 0.8,
                                                  enableAlpha: true,
                                                  displayThumbColor: true,
                                                ),
                                              ),
                                              actions: <Widget>[
                                                FlatButton(
                                                  child: const Text('Got it'),
                                                  onPressed: () {
                                                    modelRM.setState((color) {
                                                      changeColor(pickerColor);
                                                    });
                                                    Navigator.of(context).pop();
                                                  },
                                                ),
                                              ],
                                            ),
                                          );
                                        },
                                        child: Container(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.05,
                                            height: MediaQuery.of(context)
                                                    .size
                                                    .height *
                                                0.05,
                                            decoration: BoxDecoration(
                                                color: pickerColor,
                                                shape: BoxShape.circle)),
                                      ),
                                      TextField(
                                        style: TextStyle(
                                            color: Colors.blueGrey.shade200),
                                        controller: controller,
                                        maxLength: 20,
                                        decoration: InputDecoration(
                                            helperStyle: TextStyle(
                                                color:
                                                    Colors.blueGrey.shade100),
                                            hintStyle: TextStyle(
                                                color:
                                                    Colors.blueGrey.shade100),
                                            labelStyle: TextStyle(
                                                color:
                                                    Colors.blueGrey.shade200),
                                            enabledBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Colors.blueGrey),
                                            ),
                                            focusedBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Colors.blueGrey),
                                            ),
                                            border: InputBorder.none,
                                            labelText: 'Give a category name'),
                                      ),
                                      Row(children: [
                                        SizedBox(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.25,
                                          child: RaisedButton(
                                            onPressed: () {
                                              setState(() {
                                                controller.text = "";
                                              });
                                              Navigator.of(context).pop();
                                            },
                                            child: Text(
                                              "Cancel",
                                              style: TextStyle(
                                                  color: Colors.white),
                                            ),
                                            color: Colors.blueGrey[800],
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 25.0),
                                        ),
                                        SizedBox(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.25,
                                          child: RaisedButton(
                                            child: Text("Create"),
                                            color: Colors.blueGrey.shade200,
                                            onPressed: () {
                                              if (modelRM.state.names
                                                  .contains(controller.text)) {
                                                setState(() {
                                                  controller.text = "";
                                                });
                                                Navigator
                                                    .pushNamedAndRemoveUntil(
                                                        context,
                                                        '/',
                                                        (_) => false);
                                                Flushbar(
                                                  message:
                                                      "Category already exists",
                                                  icon: Icon(
                                                    Icons.error,
                                                    size: 28,
                                                    color: Colors.red.shade700,
                                                  ),
                                                  duration: const Duration(
                                                      seconds: 3),
                                                )..show(context);
                                              } else {
                                                modelRM.setState((s) async {
                                                  List<Items> temp = [];
                                                  modelRM.state.categories
                                                      .insert(0, temp);
                                                  modelRM.state.names.insert(
                                                      0, controller.text);
                                                  modelRM.state.colors
                                                      .insert(0, pickerColor);
                                                  await modelRM.state
                                                      .rebuildList();
                                                });
                                                setState(() {
                                                  controller.text = "";
                                                });
                                                Navigator.of(context).pop();
                                              }
                                            },
                                          ),
                                        )
                                      ]),
                                    ],
                                  ),
                                ),
                              ),
                            );
                          });
                    }));
        });
  }
}
