import 'package:add_2_calendar/add_2_calendar.dart';
import 'package:avatar_glow/avatar_glow.dart';
import 'package:flutter/material.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_todoapp/main.dart';
import 'package:image_picker/image_picker.dart';
import 'package:share/share.dart';
import 'package:social_share_plugin/social_share_plugin.dart';
import 'package:states_rebuilder/states_rebuilder.dart';
import 'package:speech_to_text/speech_to_text.dart' as stt;
import 'Category.dart';
import 'Items.dart';

class TODOCreate extends StatefulWidget {
  int categoryIndex;
  TODOCreate(this.categoryIndex);
  @override
  State<StatefulWidget> createState() {
    return TODOCreateState(this.categoryIndex);
  }
}

class TODOCreateState extends State<TODOCreate> {
  int categoryIndex;
  TODOCreateState(this.categoryIndex);
  stt.SpeechToText _speech;
  double confindence = 1.0;

// create some values
  Color pickerColor = Colors.black;
  Color currentColor = Colors.black;
  DateTime notificationDate = DateTime(0);
  DateTime selectedDate = DateTime.now();

// ValueChanged<Color> callback
  void changeColor(Color color) {
    setState(() => pickerColor = color);
  }

  // Controller that handles the TextField
  final TextEditingController controller = TextEditingController();
  bool _isListening = false;
  @override
  void initState() {
    super.initState();
    _speech = stt.SpeechToText();
  }

  @override
  void dispose() {
    controller.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return StateBuilder<Category>(
        observe: () => RM.get<Category>(),
        builder: (context, modelRM) {
          return Scaffold(
            backgroundColor: Theme.of(context).primaryColor,
            appBar: AppBar(
                title: Text(
                  'Create a task',
                  style: TextStyle(color: Colors.black),
                ),
                elevation: 0.0,
                leading: IconButton(
                    icon: Icon(Icons.arrow_back, color: Colors.black),
                    onPressed: () => Navigator.pushNamedAndRemoveUntil(
                        context, '/', (_) => false))),
            body: Center(
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 8.0, vertical: 0.0),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      TextField(
                          style: TextStyle(color: Colors.black),
                          cursorColor: Colors.blueGrey,
                          autofocus: false,
                          controller: controller,
                          decoration: InputDecoration(
                            hintStyle: TextStyle(color: Colors.teal),
                            labelStyle: TextStyle(color: Colors.blueGrey),
                            suffixIcon: AvatarGlow(
                              animate: _isListening,
                              glowColor: Colors.teal,
                              endRadius: 15.0,
                              duration: const Duration(milliseconds: 2000),
                              repeatPauseDuration:
                                  const Duration(milliseconds: 200),
                              repeat: true,
                              child: IconButton(
                                icon: Icon(
                                  _isListening ? Icons.mic : Icons.mic_none,
                                  color: Colors.blueGrey.shade300,
                                ),
                                onPressed: () {
                                  {
                                    modelRM.setState((s) => _listen());
                                  }
                                },
                              ),
                            ),
                            focusColor: Colors.blueGrey.shade300,
                            fillColor: pickerColor,
                            labelText: 'Enter  your task',
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.blueGrey),
                            ),
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.blueGrey),
                            ),
                          )),
                      Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              InkWell(
                                child: RaisedButton(
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(15.0),
                                      side: BorderSide(color: Colors.black12)),
                                  color: pickerColor,
                                  onPressed: () {
                                    showDialog(
                                      context: context,
                                      child: AlertDialog(
                                        title: const Text('Pick a color!'),
                                        content: SingleChildScrollView(
                                          child: ColorPicker(
                                            pickerColor: pickerColor,
                                            onColorChanged: changeColor,
                                            showLabel: true,
                                            pickerAreaHeightPercent: 0.8,
                                            enableAlpha: true,
                                            displayThumbColor: true,
                                          ),
                                        ),
                                        actions: <Widget>[
                                          FlatButton(
                                            child: const Text('Got it'),
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                            },
                                          ),
                                        ],
                                      ),
                                    );
                                  },
                                  child: Text(
                                    "Choose Color",
                                    style: TextStyle(
                                        color: Theme.of(context).primaryColor),
                                  ),
                                ),
                              ),
                              Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    IconButton(
                                        icon: Icon(
                                          (notificationDate == DateTime(0))
                                              ? Icons.alarm_add_rounded
                                              : Icons.alarm,
                                          size: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.08,
                                        ),
                                        color: (notificationDate == DateTime(0))
                                            ? (Colors.grey.shade600)
                                            : Colors.blueGrey,
                                        onPressed: () =>
                                            DatePicker.showDateTimePicker(
                                              context,
                                              showTitleActions: true,
                                              theme: DatePickerTheme(
                                                  backgroundColor: Colors.white,
                                                  itemStyle: const TextStyle(
                                                      color: Colors.black),
                                                  doneStyle: TextStyle(
                                                      color: Colors
                                                          .blueGrey.shade900,
                                                      fontWeight:
                                                          FontWeight.bold)),
                                              minTime: DateTime.now(),
                                              maxTime: DateTime(
                                                  2020, 11, 29, 05, 09),
                                              onConfirm: (date) {
                                                print('confirm $date');
                                                setState(() {
                                                  notificationDate = date;
                                                });
                                              },
                                            )),
                                    if (notificationDate != DateTime(0))
                                      IconButton(
                                        icon: Icon(
                                          Icons.remove_circle,
                                          size: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.06,
                                        ),
                                        color: Colors.blueGrey,
                                        onPressed: () {
                                          setState(() {
                                            notificationDate = DateTime(0);
                                          });
                                        },
                                      )
                                  ]),
                            ]),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 30.0),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 5.0),
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              FlatButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(12.0)),
                                color: Colors.blueGrey.shade700,
                                child: Row(
                                    mainAxisSize: MainAxisSize.min,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text("Add to calendar",
                                          style:
                                              TextStyle(color: Colors.white)),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(left: 8.0),
                                        child: Icon(
                                          Icons.calendar_today,
                                          color: Colors.white70,
                                        ),
                                      )
                                    ]),
                                onPressed: () {
                                  final Event event = Event(
                                    title: controller.text,
                                    description: "ToDo task",
                                    location: 'Event location',
                                    startDate: notificationDate != DateTime(0)
                                        ? notificationDate
                                        : DateTime.now(),
                                    endDate: notificationDate != DateTime(0)
                                        ? notificationDate
                                            .add(Duration(days: 1))
                                        : DateTime.now().add(Duration(days: 1)),
                                  );
                                  Add2Calendar.addEvent2Cal(event);
                                },
                              ),
                              RaisedButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(12.0)),
                                color: Colors.blueGrey.shade700,
                                onPressed: () {
                                  Share.share(controller.text);
                                },
                                child: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Icon(
                                      Icons.share_rounded,
                                      color: Colors.white70,
                                    ),
                                    Padding(
                                        padding: EdgeInsets.only(
                                            left: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.01)),
                                    Text("Share it!",
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold))
                                  ],
                                ),
                              )
                            ]),
                      ),
                    ]),
              ),
            ),
            floatingActionButton: FloatingActionButton(
                child: Icon(Icons.done),
                onPressed: () {
                  if (notificationDate != DateTime(0)) {
                    showNotification(
                        notificationDate.year,
                        notificationDate.month,
                        notificationDate.day,
                        notificationDate.hour,
                        notificationDate.minute,
                        "You have a task to do!",
                        controller.text);
                    modelRM.setState((s) async {
                      modelRM.state.categories[categoryIndex].add(
                        Items(
                            notificationable: true,
                            text: Text(
                              controller.text ?? "Default",
                              style: TextStyle(color: pickerColor),
                            ),
                            value: false,
                            year: notificationDate.year,
                            month: notificationDate.month,
                            minute: notificationDate.minute,
                            hour: notificationDate.hour,
                            day: notificationDate.day,
                            textCategory: modelRM.state.names[categoryIndex],
                            textColor: pickerColor.value,
                            categoryColor:
                                modelRM.state.colors[categoryIndex].value ??
                                    Colors.amberAccent.value),
                      );
                      if (modelRM.state.categories.length >= 1)
                        await modelRM.state.rebuildList();
                    });
                  } else {
                    modelRM.setState((s) async {
                      modelRM.state.categories[categoryIndex].add(Items(
                          notificationable: false,
                          text: Text(
                            controller.text ?? "Default",
                            style: TextStyle(color: pickerColor),
                          ),
                          value: false,
                          textCategory: modelRM.state.names[categoryIndex],
                          categoryColor:
                              modelRM.state.colors[categoryIndex].value ??
                                  Colors.tealAccent.value,
                          textColor: pickerColor.value));
                      if (modelRM.state.categories.length >= 1)
                        await modelRM.state.rebuildList();
                    });
                  }
                  if (controller.text.contains("Post on Twitter:") ||
                      controller.text.contains("post on twitter:") ||
                      controller.text.contains("post on Twitter:") ||
                      controller.text.contains("Post on twitter:")) {
                    final temp = controller.text.substring(16);
                    SocialSharePlugin.shareToTwitterLink(text: temp, url: "");
                  }
                  Navigator.pushNamedAndRemoveUntil(context, '/', (_) => false);
                }),
          );
        });
  }

  void _listen() async {
    if (!_isListening) {
      bool available = await _speech.initialize(
        onStatus: (val) {
          print(val);
          if (val == "notListening")
            setState(() {
              _isListening = false;
            });
          else
            print(val);
        },
        onError: (val) => print('onError: $val'),
      );
      if (available) {
        setState(() => _isListening = true);
        _speech.listen(
          onResult: (val) => setState(() {
            print(val.recognizedWords);
            controller.text = val.recognizedWords;
            if (val.hasConfidenceRating && val.confidence > 0) {
              confindence = val.confidence;
              print("Conf:$confindence");
            }
          }),
        );
      }
    } else {
      setState(() => _isListening = false);
      _speech.stop();
    }
  }
}

class TODOModify extends StatefulWidget {
  final Items item;
  final int index;
  TODOModify(this.item, this.index);
  @override
  State<StatefulWidget> createState() {
    return TODOModifyState(this.item, this.index);
  }
}

class TODOModifyState extends State<TODOModify> {
  @override
  void initState() {
    super.initState();
    controller.text = text.text.data;
    pickerColor = Color(text.textColor);
    currentColor = Color(text.textColor);
    _speech = stt.SpeechToText();
  }

  final _picker = ImagePicker();
// create some values
  stt.SpeechToText _speech;
  Items text;
  int index;
  Color pickerColor;
  Color currentColor;
  DateTime notificationDate = DateTime(0);
  TODOModifyState(this.text, this.index);
  bool _isListening = false;
// ValueChanged<Color> callback
  void changeColor(Color color) {
    setState(() {
      pickerColor = color;
    });
  }

  double confindence = 1.0;
  void _listen() async {
    if (!_isListening) {
      bool available = await _speech.initialize(
        onStatus: (val) {
          print(val);
          if (val == "notListening")
            setState(() {
              _isListening = false;
            });
          else
            print(val);
        },
        onError: (val) => print('onError: $val'),
      );
      if (available) {
        setState(() => _isListening = true);
        _speech.listen(
          onResult: (val) => setState(() {
            print(val.recognizedWords);
            controller.text = val.recognizedWords;
            if (val.hasConfidenceRating && val.confidence > 0) {
              confindence = val.confidence;
              print("Conf:$confindence");
            }
          }),
        );
      }
    } else {
      setState(() => _isListening = false);
      _speech.stop();
    }
  }

  // Controller that handles the TextField
  final TextEditingController controller = TextEditingController();
  @override
  void dispose() {
    super.dispose();
    controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return StateBuilder<Category>(
        observe: () => RM.get<Category>(),
        builder: (context, modelRM) {
          return Scaffold(
            backgroundColor: Theme.of(context).primaryColor,
            appBar: AppBar(
                elevation: 0.0,
                title: Text(
                  'Create a task',
                  style: TextStyle(color: Colors.black),
                ),
                leading: IconButton(
                    icon: Icon(
                      Icons.arrow_back,
                      color: Colors.black,
                    ),
                    onPressed: () => Navigator.pushNamedAndRemoveUntil(
                        context, '/', (_) => false))),
            body: Center(
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 8.0, vertical: 0.0),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      TextField(
                          style: TextStyle(color: Colors.black),
                          cursorColor: Colors.blueGrey,
                          autofocus: false,
                          controller: controller,
                          decoration: InputDecoration(
                            hintStyle: TextStyle(color: Colors.teal),
                            labelStyle: TextStyle(color: Colors.blueGrey),
                            suffixIcon: AvatarGlow(
                              animate: _isListening,
                              glowColor: Colors.teal,
                              endRadius: 15.0,
                              duration: const Duration(milliseconds: 2000),
                              repeatPauseDuration:
                                  const Duration(milliseconds: 200),
                              repeat: true,
                              child: IconButton(
                                icon: Icon(
                                  _isListening ? Icons.mic : Icons.mic_none,
                                  color: Colors.blueGrey.shade300,
                                ),
                                onPressed: () {
                                  {
                                    modelRM.setState((s) => _listen());
                                  }
                                },
                              ),
                            ),
                            focusColor: Colors.blueGrey.shade300,
                            fillColor: pickerColor,
                            labelText: 'Enter  your task',
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.blueGrey),
                            ),
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.blueGrey),
                            ),
                          )),
                      Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              InkWell(
                                child: RaisedButton(
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(18.0),
                                      side: BorderSide(color: Colors.black12)),
                                  color: pickerColor,
                                  onPressed: () {
                                    showDialog(
                                      context: context,
                                      child: AlertDialog(
                                        title: const Text('Pick a color!'),
                                        content: SingleChildScrollView(
                                          child: ColorPicker(
                                            pickerColor: pickerColor,
                                            onColorChanged: changeColor,
                                            showLabel: true,
                                            pickerAreaHeightPercent: 0.8,
                                            enableAlpha: true,
                                            displayThumbColor: true,
                                          ),
                                        ),
                                        actions: <Widget>[
                                          FlatButton(
                                            child: const Text('Got it'),
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                            },
                                          ),
                                        ],
                                      ),
                                    );
                                  },
                                  child: Text(
                                    "Choose Color",
                                    style: TextStyle(
                                        color: Theme.of(context).primaryColor),
                                  ),
                                ),
                              ),
                              Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    IconButton(
                                        icon: Icon(
                                          Icons.notifications,
                                          size: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.08,
                                        ),
                                        color: (text.notificationable ||
                                                notificationDate != DateTime(0))
                                            ? (Colors.teal)
                                            : Colors.grey,
                                        onPressed: () =>
                                            DatePicker.showDateTimePicker(
                                              context,
                                              showTitleActions: true,
                                              theme: DatePickerTheme(
                                                  backgroundColor:
                                                      Colors.blueGrey.shade400,
                                                  itemStyle: const TextStyle(
                                                      color: Colors.black),
                                                  doneStyle: TextStyle(
                                                      color: Colors
                                                          .blueGrey.shade900,
                                                      fontWeight:
                                                          FontWeight.bold)),
                                              minTime: DateTime.now(),
                                              maxTime: DateTime(
                                                  2020, 11, 29, 05, 09),
                                              onConfirm: (date) {
                                                print('confirm $date');
                                                setState(() {
                                                  notificationDate = date;
                                                });
                                              },
                                            )),
                                    if (text.notificationable ||
                                        notificationDate != DateTime(0))
                                      IconButton(
                                        icon: Icon(
                                          Icons.remove_circle,
                                          size: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.06,
                                        ),
                                        color: Colors.blueGrey.shade200,
                                        onPressed: () {
                                          setState(() {
                                            notificationDate = DateTime(0);
                                            text.notificationable = false;
                                          });
                                        },
                                      )
                                  ]),
                            ]),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 20.0),
                        child: FlatButton(
                          color: Colors.blueGrey.shade700,
                          child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text("Add to calendar",
                                    style: TextStyle(
                                        color: Theme.of(context).primaryColor)),
                                Padding(
                                  padding: const EdgeInsets.only(left: 8.0),
                                  child: Icon(
                                    Icons.calendar_today,
                                    color: Colors.blueGrey.shade200,
                                  ),
                                )
                              ]),
                          onPressed: () {
                            final Event event = Event(
                              title: controller.text,
                              description: "ToDo task",
                              location: 'Event location',
                              startDate: notificationDate != DateTime(0)
                                  ? notificationDate
                                  : DateTime.now(),
                              endDate: notificationDate != DateTime(0)
                                  ? notificationDate.add(Duration(days: 1))
                                  : DateTime.now().add(Duration(days: 1)),
                            );
                            Add2Calendar.addEvent2Cal(event);
                          },
                        ),
                      ),
                    ]),
              ),
            ),
            floatingActionButton: FloatingActionButton(
                child: Icon(Icons.done),
                onPressed: () {
                  modelRM.setState((s) async {
                    // ignore: missing_return
                    if (index != modelRM.state.categories.length - 1) {
                      modelRM
                          .state.categories[modelRM.state.categories.length - 1]
                          .remove(text);
                      if (notificationDate != DateTime(0)) {
                        var element1 = Items(
                          text: Text(controller.text,
                              style: TextStyle(color: pickerColor)),
                          value: false,
                          notificationable: true,
                          textCategory: modelRM.state.names[index],
                          textColor: pickerColor.value,
                          jsonText: controller.text,
                          year: notificationDate.year,
                          month: notificationDate.month,
                          minute: notificationDate.minute,
                          hour: notificationDate.hour,
                          day: notificationDate.day,
                          categoryColor: modelRM.state.colors[index].value,
                        );
                        modelRM.state.categories[index].remove(text);
                        modelRM.state.categories[index].add(element1);
                        showNotification(
                            notificationDate.year,
                            notificationDate.month,
                            notificationDate.day,
                            notificationDate.hour,
                            notificationDate.minute,
                            "You can do it! :)",
                            controller.text);
                      } else {
                        var element1 = Items(
                            text: Text(controller.text,
                                style: TextStyle(color: pickerColor)),
                            value: false,
                            notificationable: false,
                            textCategory: modelRM.state.names[index],
                            jsonText: controller.text,
                            textColor: pickerColor.value,
                            categoryColor: modelRM.state.colors[index].value);
                        modelRM.state.categories[index].remove(text);
                        modelRM.state.categories[index].add(element1);
                      }
                    }
                    //modifying element in all category
                    else {
                      final catIndex =
                          modelRM.state.names.indexOf(text.textCategory);
                      modelRM.state.categories[catIndex].remove(text);
                      modelRM.state.categories[index].remove(text);
                      if (notificationDate != DateTime(0)) {
                        var element1 = Items(
                            text: Text(controller.text,
                                style: TextStyle(color: pickerColor)),
                            value: false,
                            notificationable: true,
                            textCategory: modelRM.state.names[catIndex],
                            textColor: pickerColor.value,
                            jsonText: controller.text,
                            year: notificationDate.year,
                            hour: notificationDate.hour,
                            month: notificationDate.month,
                            minute: notificationDate.minute,
                            day: notificationDate.day,
                            categoryColor: modelRM.state.colors[index].value);
                        modelRM.state.categories[catIndex].add(element1);
                        showNotification(
                            notificationDate.year,
                            notificationDate.month,
                            notificationDate.day,
                            notificationDate.hour,
                            notificationDate.minute,
                            "You can do it! :)",
                            controller.text);
                      } else {
                        var element1 = Items(
                            text: Text(controller.text,
                                style: TextStyle(color: pickerColor)),
                            value: false,
                            notificationable: false,
                            textCategory: modelRM.state.names[catIndex],
                            textColor: pickerColor.value,
                            jsonText: controller.text,
                            categoryColor: modelRM.state.colors[index].value);
                        modelRM.state.categories[catIndex].add(element1);
                      }
                    }
                    if (controller.text.contains("Post on Twitter:") ||
                        controller.text.contains("post on twitter:") ||
                        controller.text.contains("post on Twitter:") ||
                        controller.text.contains("Post on twitter:")) {
                      final temp = controller.text.substring(16);
                      SocialSharePlugin.shareToTwitterLink(text: temp, url: "");
                    }
                    await modelRM.state.rebuildList();
                  });
                  Navigator.pushNamedAndRemoveUntil(context, '/', (_) => false);
                }),
          );
        });
  }
}

void showNotification(int year, int month, int day, int hour, int minute,
    String title, String body) async {
  await _notification(
      year: year,
      month: month,
      day: day,
      hour: hour,
      minute: minute,
      title: title,
      body: body);
}

Future<void> _notification(
    {int year,
    int month,
    int day,
    int hour,
    int minute,
    String title,
    String body}) async {
  var androidPlatformChannelSpecifics = AndroidNotificationDetails(
      'channel_ID', 'channel name', 'channel description',
      importance: Importance.Max,
      priority: Priority.Max,
      ticker: 'test ticker');

  var iOSChannelSpecifics = IOSNotificationDetails();
  var platformChannelSpecifics =
      NotificationDetails(androidPlatformChannelSpecifics, iOSChannelSpecifics);
  await flutterLocalNotificationsPlugin.schedule(body.length, title, body,
      DateTime(year, month, day, hour, minute), platformChannelSpecifics);
}
